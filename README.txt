You can find more detailed information (in pdf and manpages) in the docs/ directory

This release (0.7) consists of the following tools:

oggJoin, oggSplit, oggCut, oggCat, oggResize, oggSlideshow, oggThumb, oggLength,
oggScroll, oggDump, oggSilence

Required Software
-----------------

for oggCat oggCut oggDump oggSplit oggJoin oggLength:
NONE!! (No you do not need any ogg/theora/vorbis library for that)

for oggSlideshow, oggThumb, oggResize, oggSilence:
libogg
libtheora (>= 1.0)
libvorbis
 -> can both be found here: http://www.xiph.org/downloads/
libgd -> can be found here: http://www.libgd.org/Downloads/

for oggScroll:
libogg
libtheora (>= 1.0)
 -> can both be found here: http://www.xiph.org/downloads/
libSDL
 -> http://www.libsdl.org

If you have questions regarding the ogg video tools, write a mail
(yorn_at_gmx_dot_net) or join the developers mailing list at

https://lists.sourceforge.net/lists/listinfo/oggvideotools-users

Have fun
Yorn
