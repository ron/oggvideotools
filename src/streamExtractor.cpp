#include "streamExtractor.h"

ExtractorInformation::ExtractorInformation()
    : type(ogg_unknown), serialNo(0), parameter(0), numOfHeaderPackets(0)
{
}

ExtractorInformation::ExtractorInformation(const ExtractorInformation& extractorInfo)
    : type(extractorInfo.type), serialNo(extractorInfo.serialNo),
    parameter(0), numOfHeaderPackets(extractorInfo.numOfHeaderPackets)
{
  if (extractorInfo.parameter)
    parameter = extractorInfo.parameter->clone();

}

ExtractorInformation& ExtractorInformation::operator=(const ExtractorInformation& extractorInfo)
{
  type               = extractorInfo.type;
  serialNo           = extractorInfo.serialNo;
  numOfHeaderPackets = extractorInfo.numOfHeaderPackets;

  if (parameter)
    delete parameter;

  if (extractorInfo.parameter)
    parameter = extractorInfo.parameter->clone();
  else
    parameter = 0;

  return(*this);
}

ExtractorInformation::~ExtractorInformation()
{
  if (parameter)
    delete parameter;
}

StreamExtractor::StreamExtractor()
{
}

StreamExtractor::~StreamExtractor()
{
}

